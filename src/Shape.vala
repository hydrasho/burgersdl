using Bg;
namespace Bg{

	public class Shape : Drawable{
		public Shape(){
			m_whats_is = SHAPE;
			m_size.x = 1;
			m_size.y = 1;
		}
		public void set_texture(Texture texture){
			m_texture = texture;
			m_rect = {0,0, m_texture.w, m_texture.h};
		}
	}
}
