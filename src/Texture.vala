using Bg;
namespace Bg{

	public class Texture{
		public Texture(string? file = null){
			if(file == null){
				return;
			}
			this.set_texture(file);

		}
		public bool set_texture(string file){
			m_surface = new SDL.Video.Surface.from_bmp(file);
			if(m_surface == null){
				m_surface = SDLImage.load(file);
			}
			if(m_surface == null){
				print(@"Erreur texture format pour $(file)\n");
				return true;
			}
			return false;
		}

		public void set_color(Color c){
			m_surface.set_colormod(c.r ,c.g, c.b);
		}

		public Color get_color(){
			Color color = {};
			m_surface.get_colormod(out color.r, out color.g, out color.b);
			return color;
		}

		public void set_opacity(int alpha){
			m_surface.set_alphamod((uint8)alpha);
		}

		public SDL.Video.Surface *get_surface(){
			return m_surface;
		}

		public void set_surface(ref SDL.Video.Surface surface){
			m_surface = (owned)surface;
		}

		public int h {
			get {return m_surface.h;}
			private set{;}
		}
		public int w {
			get {return m_surface.w;}
			private set{;}
		}

		private SDL.Video.Surface m_surface;
	}
}
