# BurgerSDL

## Exemple:
```
using Bg;

 void main(){
    var win = new Window("Super Titre");
    var texture = new Texture("hello.bmp");
    var sprite = new Sprite(texture);
    var event = Event.get_loop_event();

    /* ###  Déclaration des événements   ### */
    event.onClose.connect(win.close);

    event.onClick.connect((x,y) => {
        print(@" Clique $x et $y\n");
    });
    /* ######      Boucle de dessin      ###### */
    while(win.is_open())
    {
        win.clear();
     	win.draw(sprite);
        win.present();
    }
 }
```
