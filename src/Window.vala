using Bg;
namespace Bg{

	const int POS_CENTER = (int)SDL.Video.Window.POS_CENTERED;

	public class Window{
		public Window(string title="titre", int taillex=500, int tailley=500){
			m_win = new SDL.Video.Window(title, POS_CENTER, POS_CENTER, taillex, tailley, SDL.Video.WindowFlags.OPENGL);
			SDL.Video.GL.set_attribute(SDL.Video.GL.Attributes.MULTISAMPLEBUFFERS, 8);
			m_render = SDL.Video.Renderer.create(m_win, -1, SDL.Video.RendererFlags.ACCELERATED);
			m_event = new Event();
			is_open = true;
		}
		
		public void draw(Drawable drawer){
		    if(drawer.whats_is == SPRITE){
				drawer.set_render(m_render);
				Texture text_tmp = drawer.get_texture();
				Rect rect = drawer.rect;
				Vector2i pos = {(int) drawer.position.x, (int) drawer.position.y};
				if(rect.w == -1){
				    drawer.rect = {0, 0, text_tmp.w, text_tmp.h};
				    rect = drawer.rect;
				}
				m_render->copyex(drawer.get_texture_sdl(), {rect.x, rect.y, rect.w, rect.h}, {pos.x, pos.y, (int)(rect.w * drawer.size.x) ,(int)(rect.h * drawer.size.y)}, drawer.angle, {text_tmp.w/2,text_tmp.h/2}, SDL.Video.RendererFlip.NONE);

			}
		}
		
		public void present(){
            if(m_fps_limit != -1)
			    fps_loop();
			m_render->present();
		}
		
		public void clear(){
			m_render->set_draw_color(255, 255, 255, 250);
			m_render->fill_rect ({0, 0, 1024, 768});
		}
		
		public void close(){
			is_open = false;
		}

		protected void fps_loop(){
			uint32 time_dif,slp;

			slp = 1000/m_fps_limit;
			slp +=1;
			time_end = SDL.Timer.get_ticks();
			time_dif = time_end - time_begin;

			if(time_dif <= 0) time_dif = 0;
			if(time_dif < slp)
				SDL.Timer.delay(slp - time_dif);
			time_begin = SDL.Timer.get_ticks();
		}
		public void set_framelimit(uint fps){
			m_fps_limit = (int32)fps;
		}


		public uint32 get_fps_average(){
			return (uint32)5;
		}


		~Window(){
			delete m_render;
		}
        
        private int32 m_fps_limit = 120;
		private uint32 time_end;
		private uint32 time_begin;        
		public bool is_open{get; private set;}
		private Event m_event;
		private SDL.Video.Window m_win;
		private SDL.Video.Renderer *m_render;
	}
}
